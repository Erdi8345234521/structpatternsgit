// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "IKHandlingComponent.generated.h"

class USkeletalMeshSocket;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class STRUCTPATTERNS_API UIKHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	USkeletalMeshSocket* GetInteractionSocket() const { return nullptr; };

	void UseSocketTransformBegin(USkeletalMeshSocket* TargetSocket){};
	
	void UseSocketTransformEnd(USkeletalMeshSocket* TargetSocket){};
		
};
