// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "XMLDocument.h"

class XMLDocument;

/**
 * Our proxy object that we'll use
 * as a proxy for third-party service lib.
 */
class STRUCTPATTERNS_API XMLProxy final
{
	using cStr = const FString;
	using DocMap = TMap<cStr /* Name */, XMLDocument /* Document */>;
	
public:
	/** @return Read int value from XML document */
	int ReadInt(cStr& Doc, cStr& Name);

	/** @return Read double value from XML document */
	double ReadDouble(cStr& Doc, cStr& Name);

	/** @return Read string value from XML document */
	FString ReadString(cStr& Doc, cStr& Name);

	/** @return Write int value to XML document */
	void WriteInt(cStr& Doc, cStr& Name, int Value);

	/** @return Write double value to XML document */
	void WriteDouble(cStr& Doc, cStr& Name, double Value);

	/** @return Write string value to XML document */
	void WriteString(cStr& Doc, cStr& Name, const char* Value);

protected:
	/** Parse document and add it to the map */
	void ParseDoc(cStr& Name);

private:
	/** Map of document names and instances */
	DocMap Documents;
};

inline int XMLProxy::ReadInt(cStr& Doc, cStr& Name)
{
	if(!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	return Documents[Doc].ReadInt(TCHAR_TO_ANSI(*Name));
}

inline double XMLProxy::ReadDouble(cStr& Doc, cStr& Name)
{
	if (!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	return Documents[Doc].ReadDouble(TCHAR_TO_ANSI(*Name));
}

inline FString XMLProxy::ReadString(cStr& Doc, cStr& Name)
{
	if (!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	return Documents[Doc].ReadString(TCHAR_TO_ANSI(*Name));
}

inline void XMLProxy::WriteInt(cStr& Doc, cStr& Name, int Value)
{
	if(!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	Documents[Doc].WriteInt(TCHAR_TO_ANSI(*Name), Value);
}

inline void XMLProxy::WriteDouble(cStr& Doc, cStr& Name, double Value)
{
	if (!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	Documents[Doc].WriteDouble(TCHAR_TO_ANSI(*Name), Value);
}

inline void XMLProxy::WriteString(cStr& Doc, cStr& Name, const char* Value)
{
	if (!Documents.Contains(Doc))
	{
		ParseDoc(Doc);
	}

	Documents[Doc].WriteString(TCHAR_TO_ANSI(*Name), Value);
}

inline void XMLProxy::ParseDoc(cStr& Name)
{
	const auto Doc = XMLDocument::ParseDoc(TCHAR_TO_ANSI(*Name));
	if(Doc.IsValid())
	{
		Documents.Add(Name, Doc);
	}
}
